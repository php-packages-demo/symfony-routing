# [symfony](https://packagist.org/packages/symfony/)/[routing](https://packagist.org/packages/symfony/routing)

[**symfony/routing**](https://packagist.org/packages/symfony/routing)
![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/routing)
The Routing component maps an HTTP request to a set of configuration variables. [symfony.com/routing](https://symfony.com/routing)

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=php-nikic-fast-route+php-symfony-routing+php-horde-routes+php-illuminate-routing&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/routing) symfony/routing
* ![Packagist Downloads](https://img.shields.io/packagist/dm/nikic/fast-route) nikic/fast-route
* ![Packagist Downloads](https://img.shields.io/packagist/dm/illuminate/routing) illuminate/routing
* ![Packagist Downloads](https://img.shields.io/packagist/dm/horde/routes) horde/routes

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=php-symfony-filesystem+php-symfony-console+php-symfony-process+php-symfony-finder+php-symfony-expression-language+php-symfony-cache+php-symfony-config+php-symfony-dependency-injection+php-symfony-event-dispatcher&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/console) symfony/console
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/finder) symfony/finder
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/process) symfony/process
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/event-dispatcher) symfony/event-dispatcher
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/routing) symfony/routing
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/filesystem) symfony/filesystem
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/config) symfony/config
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/dependency-injection) symfony/dependency-injection
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/cache) symfony/cache
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/expression-language) symfony/expression-language

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=php-symfony-event-dispatcher+php-symfony-routing+php-symfony-http-foundation+php-symfony-http-kernel&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=1&want_legend=1&want_ticks=1&from_date=2014-01-01&to_date=&hlght_date=&date_fmt=%25Y)

* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/event-dispatcher) symfony/event-dispatcher
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/http-foundation) symfony/http-foundation
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/routing) symfony/routing
* ![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/http-kernel) symfony/http-kernel


# Official documentation
* Create your own PHP Framework:
  [*The Routing Component*
  ](https://symfony.com/doc/current/create_framework/routing.html)

# Unofficial documentation
* [*Symfony - Routing*
  ](https://www.tutorialspoint.com/symfony/symfony_routing.htm)
  (Tutorials Point)
---
* [*A better ADR pattern for your Symfony controllers*
  ](https://www.strangebuzz.com/en/blog/a-better-adr-pattern-for-your-symfony-controllers)
  2024-11 COil (Strangebuzz)
* [*Handling deprecated routes using Symfony compiler passes*
  ](https://antonio-turdo.medium.com/handling-deprecated-routes-using-symfony-compiler-passes-38fbf48c4cf7)
  2023-08 Antonio Turdo @ Medium
* [*Switch Symfony String Route Names to Constants*
  ](https://getrector.org/blog/2021/01/11/switch-symfony-string-route-names-to-constants)
  2021-01 Rector

## French section
* (fr) [*CMS en Symfony : le routing*
  ](https://dev.to/gbtux/cms-en-symfony-le-routing-2ngo)
  2022-02 Guillaume

# Performance and [nikic](https://phppackages.org/s/nikic)/[fast-route](https://phppackages.org/p/nikic/fast-route)
* [*Real World Benchmark PHP Routing*
  ](http://kaloyan.info/writing/2021/05/31/benchmark-php-routing.html)
  2021-05 Kaloyan K. Tsvetkov
* [*Making Symfony’s Router 77.7x faster - 1/2*
  ](https://medium.com/@nicolas.grekas/making-symfonys-router-77-7x-faster-1-2-958e3754f0e1)
  2018
* [*Fast request routing using regular expressions*
  ](https://nikic.github.io/2014/02/18/Fast-request-routing-using-regular-expressions.html)
  2014 
  <br/>
  This article describes a number of techniques to improve performance of
  regular expression based dispatch processes, as used in request routing or lexing.
